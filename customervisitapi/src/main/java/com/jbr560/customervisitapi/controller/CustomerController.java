package com.jbr560.customervisitapi.controller;
import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr560.customervisitapi.service.CustomerService;
import com.jbr560.customervisitapi.model.Customer;

@RestController
@CrossOrigin
public class CustomerController {
    @GetMapping("/customers")
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> allCustomers = CustomerService.getAllCustomers();
        return allCustomers;
    }
}

