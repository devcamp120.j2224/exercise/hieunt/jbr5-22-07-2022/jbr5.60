package com.jbr560.customervisitapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.*;

import com.jbr560.customervisitapi.service.VisitService;
import com.jbr560.customervisitapi.model.Visit;

@CrossOrigin
@RestController
public class VisitController {
    @GetMapping("/visits")
    public ArrayList<Visit> getAllVisits() {
        ArrayList<Visit> allVisits = VisitService.getAllVisits();
        return allVisits;
    }
}
