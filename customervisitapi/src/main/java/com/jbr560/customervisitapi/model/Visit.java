package com.jbr560.customervisitapi.model;

import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;
    public Visit(Customer customer, Date date) {
        this.customer = customer;
        this.date = date;
    }
    public String getCustomerName() {
        return customer.getName();
    }
    public double getServiceExpense() {
        return serviceExpense;
    }
    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }
    public double getProductExpense() {
        return productExpense;
    }
    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }
    public double getTotal() {
        return serviceExpense + productExpense ;
    }
    @Override
    public String toString() {
        return "Visit[customer=" + customer
            + ", date=" + date
            + ", productExpense=" + productExpense
            + ", serviceExpense=" + serviceExpense + "]";
    }  
}
