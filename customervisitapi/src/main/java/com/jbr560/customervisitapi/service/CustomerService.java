package com.jbr560.customervisitapi.service;

import java.util.ArrayList;
import org.springframework.stereotype.Service;

import com.jbr560.customervisitapi.model.Customer;

@Service
public class CustomerService {    
    private static ArrayList<Customer> allCustomers = new ArrayList<>();
    private static Customer khach1 = new Customer("Hoa");
    private static Customer khach2 = new Customer("Lan");
    private static Customer khach3 = new Customer("Nam");
    static {
        allCustomers.add(khach1);
        khach2.setMember(true);
        khach2.setMemberType("Silver");
        allCustomers.add(khach2);
        khach3.setMember(true);
        khach3.setMemberType("Gold");
        allCustomers.add(khach3);
    }
    public static ArrayList<Customer> getAllCustomers() {
        return allCustomers;
    }
    public static Customer getKhach1() {
        return khach1;
    }
    public static Customer getKhach2() {
        return khach2;
    }
    public static Customer getKhach3() {
        return khach3;
    }
}
