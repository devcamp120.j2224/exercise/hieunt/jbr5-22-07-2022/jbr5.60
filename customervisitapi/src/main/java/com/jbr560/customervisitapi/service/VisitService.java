package com.jbr560.customervisitapi.service;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.jbr560.customervisitapi.model.Visit;


@Service
public class VisitService {
    private static ArrayList<Visit> allVisits = new ArrayList<>();
    static {
        Visit visit1 = new Visit(CustomerService.getKhach1(), new Date());
        Visit visit2 = new Visit(CustomerService.getKhach2(), new Date());
        Visit visit3 = new Visit(CustomerService.getKhach3(), new Date());
        visit1.setServiceExpense(1000);
        visit1.setProductExpense(1500);
        visit2.setServiceExpense(2000);
        visit2.setProductExpense(2500);
        visit3.setServiceExpense(3000);
        visit3.setProductExpense(3500);
        allVisits.add(visit1);
        allVisits.add(visit2);
        allVisits.add(visit3);
    }
    public static ArrayList<Visit> getAllVisits() {
        return allVisits;
    }
}
